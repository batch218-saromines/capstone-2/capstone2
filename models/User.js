

const mongoose = require('mongoose')

const user_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First Name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last Name is required.']
	},
	email: {
		type: String,
		required: [true, 'Email is required.']
	},
	mobileNo: {
		type: String,
		required: [true, 'Mobile Number is required.']
	},
	password: {
		type: String,
		required: [true, 'Password is required.']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			productId: {
				productName: String,
				quantity: Number
			},
			totalAmount: {
				type: Number,
				default: ""
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
			
		}
	]
})

module.exports = mongoose.model('User', user_schema)
