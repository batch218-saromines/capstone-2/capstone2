const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({

    userId : {
        type: String,
        required:[true,"User ID is required!"]
    },

    products:[{
        productId:{
        type:String,
        required:[true, "Product ID is required!"]
        },

        productName:{
            type:String,
        },

        quantity:{
            type:Number,
            required:[true,"Quantity is required!"]
        }
    },
    ],

    totalAmount:{
        type:Number  
    },
    
    purchasedOn:{
        type:Date,
        default: new Date()
}
})

module.exports = mongoose.model('Order', order_schema)
