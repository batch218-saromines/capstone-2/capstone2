const mongoose = require('mongoose')

const product_schema = new mongoose.Schema({
    productName: {
        type: String,
        required: [true, 'Product name is required.']
    },
    description: {
        type: String,
        required: [true, 'Description name is required.']
    },
    price: {
        type: Number,
        required: [true, 'Price is required.']
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            orderId: {
                type: String,
                required: [true, 'orderId is required.']
            }
        }
    ]
})

module.exports = mongoose.model('Product', product_schema)
