// START IMPORT DEPENDENCIES
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');
const dotenv = require('dotenv');
// END IMPORT DEPENDENCIES



const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')
// const orderRoutes = require('./routes/orderRoutes')
dotenv.config()


// SERVER SETUP
const app = express();
const port = 5000


// MIDDLEWARES
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended:true}));


//MONGODB CONNECTION
mongoose.connect("mongodb+srv://admin:admin@ecommercecapstone2.ptvfb9b.mongodb.net/?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Connected to Capstone 2 Database'));

let db = mongoose.connection; 
db.on('error',console.error.bind(console, "MongoDB Connection Error.")); 
db.once('open',()=>console.log("Connected to MongoDB."))

// ROUTES
app.use("/users", userRoutes);
app.use("/products", productRoutes);
// app.use("/orders", orderRoutes);
// Routes END



// ---------------------------------
app.listen(process.env.PORT || 5000, () =>
	{console.log(`API is now online on port ${process.env.PORT || 5000}`)});
