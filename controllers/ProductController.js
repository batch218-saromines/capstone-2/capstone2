// --------------------------------------------------------------
// START IMPORT DEPENDENCIES
const mongoose = require("mongoose");
const Product = require('../models/Product')
// END IMPORT DEPENDENCIES
// --------------------------------------------------------------



// -------------------------------------------------------------
// START CREATING/ADDING PRODUCT FUNCTIONALITY - ADMIN ONLY ( MINIMUM REQUIREMENTS #3)
module.exports.addProduct = (reqBody, newData) => {
if(newData.isAdmin == true){
    let newProduct = new Product ({
        productName: reqBody.productName,
        description: reqBody.description,
        price: reqBody.price

    })
    return newProduct.save().then((newProduct, error) => {
        if(error){
                return error;
        }
        else{
            return newProduct;
        }
    })
}else{
    let message = Promise.resolve('User must be ADMIN to add a new product');
        return message.then((value) => {return value})
    }
}
// END CREATING/ADDING PRODUCT FUNCTIONALUTY - ADMIN ONLY ( MINIMUM REQUIREMENTS #3)
// -------------------------------------------------------------


// --------------------------------------------------------------
// START RETRIEVING ACTIVE PRODUCTS (MINIMUM REQUIREMENTS #4)
module.exports.showActiveProduct = () => {
    return Product.find({isActive: true}).then((result) => {
        return result
    })
}
// END RETRIEVING ACTIVE PRODUCTS (MINIMUM REQUIREMENTS #4)
// -------------------------------------------------------------






// --------------------------------------------------------------
// START RETRIEVING SINGLE PRODUCT (MINIMUM REQUIREMENTS #5)
module.exports.showProduct = (product_id) => {
    return Product.findById(product_id).then((result) => {
        return result 
    })
}

// END RETRIEVING SINGLE PRODUCT (MINIMUM REQUIREMENTS #5)
// -------------------------------------------------------------


// --------------------------------------------------------------
// START RETRIEVING ALL PRODUCTS 
module.exports.showAllProduct = () => {
    return Product.find().then((result) => {
        return result
    })
}
// END RETRIEVING ALL PRODUCTS 
// -------------------------------------------------------------



// --------------------------------------------------------------
// START UPDATING PRODUCT INFORMATION (ADMIN ONLY) (MINIMUM REQUIREMENTS #6)
module.exports.updateProduct = (data,product_id, new_data) => {
    if(data.isAdmin){
    return Product.findByIdAndUpdate(product_id, {
        name: new_data.name,
        description: new_data.description,
        price: new_data.price
    }).then((updated_product, error) => {
        if(error){
            return false
        }

        return {
            message: 'Product information has been updated successfully!'
            }
    })
}
    let message = Promise.resolve({
        message: 'Only an ADMIN can access this feature.'
    })
    return message.then((value) => {
        return value
    })
}

// END UPDATING PRODUCT INFORMATION (ADMIN ONLY) (MINIMUM REQUIREMENTS #6)
// -------------------------------------------------------------


// --------------------------------------------------------------
// START ARCHIVING  PRODUCTS (ADMIN ONLY) (MINIMUM REQUIREMENTS #7)
module.exports.archiveProduct = (data,product_id, new_data) => {
    if(data.isAdmin){
    return Product.findByIdAndUpdate(product_id, {
        isActive: new_data.isActive
    }).then((archived_product, error) => {
        if(error){
            return false
        }

        return {
            message: 'Product has been archived successfully!'
        }
    })
}
    let message = Promise.resolve({
        message: 'Only an ADMIN can access this feature.'
    })
    return message.then((value) => {
        return value
    })
}

// END ARCHIVING  PRODUCTS (ADMIN ONLY) (MINIMUM REQUIREMENTS #7)
// --------------------------------------------------------------



// --------------------------------------------------------------
// ADDITIONAL FEATURES
// START RETRIEVING ALL PRODUCTS - ADMIN ONLY
module.exports.getAllProducts = async () => {
    try{
        return await Product.find({},{image:0}).then(result=>result);
    }catch(err){
        return err
    }
}
// END RETRIEVING ALL PRODUCTS - ADMIN ONLY

