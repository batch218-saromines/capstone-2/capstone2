// --------------------------------------------------------------
// START IMPORT DEPENDENCIES
const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')


const bcrypt = require('bcrypt')
const auth = require('../auth')
// END IMPORT DEPENDENCIES
// --------------------------------------------------------------



// -------------------------------------------------------------
// START USER REGISTRATION (MINIMUM REQUIREMENTS #1)
module.exports.registerUser = (userInfo) => {
	let newUser = new User({
		firstName : userInfo.firstName,
		lastName : userInfo.lastName,
		mobileNo : userInfo.mobileNo,
		email : userInfo.email,
		password : bcrypt.hashSync(userInfo.password, 10),

	});

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			user.password = "*****";
			return user;
		};
	});
};


// END USER REGISTRATION (MINIMUM REQUIREMENTS #1)
// --------------------------------------------------------------




// --------------------------------------------------------------
// START USER LOGIN AUTHENTICATION (MINIMUM REQUIREMENTS #2)
module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if(result == null){
			return {
				message: "User doesn't exist!"
			}
		}
		const is_password_correct = bcrypt.compareSync(data.password, result.password)
		if(is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}
		return {
			message: 'Password is incorrect!'
			}
		})
	}
// END USER LOGIN AUTHENTICATION (MINIMUM REQUIREMENTS #2)
// --------------------------------------------------------------


// -------------------------------------------------------------
// START CREATE ORDER - NON ADMIN USER CHECKOUT (MINIMUM REQUIREMENTS #8)

module.exports.createOrder = async (data) => {

	let isUserUpdated = await User.findById(data.userId).then(user => {

		user.items.push({productId: data.productId});

		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else{
				return true;
			}
		})

	})

	let isProductUpdated = await Course.findById(data.productId).then(product => {

		product.purchaser.push({userId : data.userId});

		return product.save().then((product, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})

	if(isUserUpdated && isProductUpdated) {
		return true;
	} else {
		return false;
	}

};



// END CREATE ORDER - NON ADMIN USER CHECKOUT (MINIMUM REQUIREMENTS #8)
// -------------------------------------------------------------




// ----------------------------------------------------------------
// START RETRIEVING USER DETAILS (MINIMUM REQUIREMENTS #9)
	module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, {password: 0}).then((result) => {
		return result
		})
	}

// END RETRIEVING USER DETAILS (MINIMUM REQUIREMENTS #9)
// --------------------------------------------------------------











// ----------------------------------------------------------------
// START SETTING USER AS ADMIN (STRETCH GOAL #1)
	module.exports.adminAccessRequest = (data,user_id, new_data) => {
    if(data.isAdmin){
    
    return User.findByIdAndUpdate(user_id, {
        isAdmin: new_data.isAdmin
    		}).then((adminAccessRequestGranted_product, error) => {
        		if(error){
            	return false
        	}
        return {
            message: 'User Access has been updated. You can access admin features.'
        	}
    	})
	}

    let message = Promise.resolve({
        message: 'Only ADMINS can request for this feature.'
    })

    return message.then((value) => {
        return value
    	})
}

// END SETTING USER AS ADMIN (STRETCH GOAL #1)
// ----------------------------------------------------------------