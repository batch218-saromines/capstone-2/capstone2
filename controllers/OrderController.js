// --------------------------------------------------------------
//  START IMPORT DEPENDENCIES
const User = require('../models/User')
const Product = require('../models/Product')
const Order = require('../models/Order')

const bcrypt = require('bcrypt')
const auth = require('../auth')
// END DEPENDENCIES
// --------------------------------------------------------------








// --------------------------------------------------------------
// START RETRIEVE ALL ORDERS - ADMIN ONLY (STRETCH GOAL #3)
module.exports.showAllOrders = (data) => {
    if(data.isAdmin){

    return Order.find().then((result, error) => {
        if(error){
        return false
        }
        return result

    })
}
           let message = Promise.resolve({
                message: 'You must be an ADMIN to access this.'
            })
            return message.then((value) => {
                return value
            })
        }

// END RETRIEVE ALL ORDERS - ADMIN ONLY (STRETCH GOAL #3)
// --------------------------------------------------------------





// -------------------------------------------------------------
// START RETRIEVE AUTHENTICATED USER'S ORDERS (STRETCH GOAL #2)
        module.exports.userOrders = (userId) => {

        return Order.find({userId: userId}).then(result => {
            if(result.length > 0){
                return result
            }
            return {
                message: "No orders found!"
            }
        })
    }

// END RETRIEVE AUTHENTICATED USER'S ORDERS (STRETCH GOAL #2)
// --------------------------------------------------------------
