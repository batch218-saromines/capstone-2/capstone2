// --------------------------------------------------------------
// START IMPORT DEPENDENCIES
const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require("../auth.js");
// END IMPORT DEPENDENCIES
// --------------------------------------------------------------


// --------------------------------------------------------------
// START CREATING/ADDING PRODUCT FUNCTIONALITY - ADMIN ONLY ( MINIMUM REQUIREMENTS #3)
router.post("/addProduct", auth.verify, (req, res) =>{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	ProductController.addProduct(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

// END CREATING/ADDING PRODUCT FUNCTIONALITY - ADMIN ONLY ( MINIMUM REQUIREMENTS #3)
// --------------------------------------------------------------



router.post("/order", auth.verify, (req, res) =>{
	const newData = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	ProductController.addOrder(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})











// --------------------------------------------------------------
// START RETRIEVING ACTIVE PRODUCTS (MINIMUM REQUIREMENTS #4)
router.get('/active', auth.verify, (request, response) => {
	ProductController.showActiveProduct().then((result) => {
		response.send(result)
	})
})
// END RETRIEVING ACTIVE PRODUCTS (MINIMUM REQUIREMENTS #4)
// --------------------------------------------------------------



// --------------------------------------------------------------
// START RETRIEVING SINGLE PRODUCT (MINIMUM REQUIREMENTS #5)
router.get('/:productId', auth.verify, (request, response) => {
	ProductController.showProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})
// END RETRIEVING SINGLE PRODUCT (MINIMUM REQUIREMENTS #5)
// --------------------------------------------------------------



// --------------------------------------------------------------
// START RETRIEVING ALL PRODUCTS 
router.get('/active', auth.verify, (request, response) => {
	ProductController.showActiveProduct().then((result) => {
		response.send(result)
	})
})
// END RETRIEVING ALL PRODUCTS 
// --------------------------------------------------------------



// --------------------------------------------------------------
// START UPDATING PRODUCT INFORMATION (ADMIN ONLY) (MINIMUM REQUIREMENTS #6)
router.patch('/:productId/update', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	ProductController.updateProduct(data, request.params.productId, request.body).then((result) => {
		response.send(result)
	})
})

// END UPDATING PRODUCT INFORMATION (ADMIN ONLY) (MINIMUM REQUIREMENTS #6)
// -------------------------------------------------------------



// --------------------------------------------------------------
// START ARCHIVING  PRODUCTS (ADMIN ONLY) (MINIMUM REQUIREMENTS #7)
router.patch('/:productId/archive', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	ProductController.archiveProduct(data, request.params.productId, request.body).then((result) => {
		response.send(result)
	})
})
// END ARCHIVING  PRODUCTS (ADMIN ONLY) (MINIMUM REQUIREMENTS #7)
// --------------------------------------------------------------







// --------------------------------------------------------------
// ADDITIONAL FEATURES
// START RETRIEVING ALL PRODUCTS - ADMIN ONLY
router.get('/allProducts', auth.verify, async (req, res) => {
    try{
        await getAllProducts().then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})


// END RETRIEVING ALL PRODUCTS - ADMIN ONLY

module.exports = router 
