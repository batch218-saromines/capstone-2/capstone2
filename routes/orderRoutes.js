// --------------------------------------------------------------
//  START IMPORT DEPENDENCIES
const express = require('express')
const router = express.Router()
const OrderController = require('../controllers/OrderController')
const auth = require('../auth')
// END IMPORT DEPENDENCIES
// --------------------------------------------------------------



// --------------------------------------------------------------
// START CREATE ORDER - NON ADMIN USER CHECKOUT (MINIMUM REQUIREMENTS #8)
router.post("/order", auth.verify, (req, res) => {

	let data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId
	}

	userController.order(data).then(resultFromController => res.send(resultFromController));

});

// END CREATE ORDER - NON ADMIN USER CHECKOUT (MINIMUM REQUIREMENTS #8)
// --------------------------------------------------------------





// --------------------------------------------------------------
// START RETRIEVE ALL ORDERS - ADMIN ONLY (STRETCH GOAL #3)
router.get('/allOrders', auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	OrderController.showAllOrders(data).then((result) => {
		response.send(result)
	})
})

// END RETRIEVE ALL ORDERS - ADMIN ONLY (STRETCH GOAL #3)
// --------------------------------------------------------------






// --------------------------------------------------------------
// START RETRIEVE AUTHENTICATED USER'S ORDERS (STRETCH GOAL #2)

router.get('/user-order', auth.verify, (request, response) => {

	const userId = auth.decode(request.headers.authorization).id

	OrderController.userOrders(userId).then(result => {
		response.send(result)
	})

})
// END RETRIEVE AUTHENTICATED USER'S ORDERS (STRETCH GOAL #2)
// --------------------------------------------------------------







module.exports = router
