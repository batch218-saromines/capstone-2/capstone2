// --------------------------------------------------------------
// START IMPORT DEPENDENCIES
const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')
// END IMPORT DEPENDENCIES
// --------------------------------------------------------------



// --------------------------------------------------------------
// START USER REGISTRATION (MINIMUM REQUIREMENTS #1)
router.post("/register", (request, response) => {
	UserController.registerUser(request.body).then((result) => {
		response.send(result)
	})
})

// END USER REGISTRATION (MINIMUM REQUIREMENTS #1)
// --------------------------------------------------------------



// --------------------------------------------------------------
// START USER LOGIN AUTHENTICATION (MINIMUM REQUIREMENTS #2)

router.post("/login", (request, response) => {
	UserController.login(request.body).then((result) => {
		response.send(result)
	})
})
// END USER LOGIN AUTHENTICATION (MINIMUM REQUIREMENTS #2)
// --------------------------------------------------------------





// --------------------------------------------------------------
// START RETRIEVING USER DETAILS (MINIMUM REQUIREMENTS #9)
router.get("/:id/details", auth.verify, (request, response) => {
	UserController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})
// END RETRIEVING USER DETAILS (MINIMUM REQUIREMENTS #9)
// --------------------------------------------------------------



// ----------------------------------------------------------------
// START SETTING USER AS ADMIN (STRETCH GOAL #1)
router.patch('/:userId/adminRequest', auth.verify, (request, response) => {
	const data = {
		user: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	UserController.adminAccessRequest(data, request.params.userId, request.body).then((result) => {
		response.send(result)
	})
})

// ----------------------------------------------------------------
// END SETTING USER AS ADMIN (STRETCH GOAL #1)









// -------------------------------------------------------------
// START CREATE ORDER - NON ADMIN USER CHECKOUT (MINIMUM REQUIREMENTS #8)
router.post("/order", auth.verify, (req, res) => {
    let data = {
        userId: auth.decode(req.headers.authorization).id,
        productId: req.body.productId
    }

    UserController.createOrder(data).then(result => res.send(result));
});


// -------------------------------------------------------------
// END CREATE ORDER - NON ADMIN USER CHECKOUT (MINIMUM REQUIREMENTS #8)


module.exports = router
